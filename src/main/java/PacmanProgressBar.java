public class PacmanProgressBar {

    public static void main(String[] args) {
        printProgress();
    }

    public static String repeatString(int length, char character) {
        String repeated = "";

        // place a meal for pacman every x characters
        // TODO default x to 2, but be able to change it if given the correct parameter on startup
        int mealCounter = 0;
        for (int i = 0; i < length; i++) {
            // change mealCounter == to desired value
            if (mealCounter != 2) {
                repeated += character;
                mealCounter++;
            } else {
                repeated += 'o';
                mealCounter = 0;
            }
        }
        return repeated;
    }

    public static String formatPercentage(double percentage) {

        if (percentage < 10) {
            return "0" + (int) percentage + "%";
        } else {
            return (int) percentage + "%";
        }
    }

    public static String formatBar(String bar, double percentage) {
        return "[" + bar + "] " + formatPercentage(percentage);
    }

    public static int findClosestNumber(int length) {
        if (length < 5) {
//            System.out.println("Length has to be at least 5, fixing it for you.");
            length = 5;
        }

        // to make a nice bar the length has to be equal to (2 + (n * 3)), so either 5, 8, 11, 14, ...
        // so if the length is not equal to this, we need to find the closest one
        int lengthFinderUpper = 5;
        for (int i = 1; lengthFinderUpper < length; i++) {
            if (lengthFinderUpper - 5 < length) {
                lengthFinderUpper = 5 + (i * 3);
            }
        }
        int lengthFinderBottom = lengthFinderUpper - 3;

//        int oldLength = length;

        if (length - lengthFinderUpper < lengthFinderBottom - length) {
            length = lengthFinderBottom;
        } else {
            length = lengthFinderUpper;
        }

//        if (oldLength != length) {
//            String corrected = String.format("Corrected %d to %d", oldLength, length);
//            System.out.println(corrected);
//        }

        return length;
    }

    public static String[] pacmanProgress(int length, char character) {
        StringBuilder progressBar = new StringBuilder(repeatString(length, character));
        String[] allProgress = new String[length + 2];

        // this part makes pacman alternate between opening and closing his mouth
        for (int pacmanLocation = 0; pacmanLocation < allProgress.length - 2; pacmanLocation++) {
            char pacman = 'C';
            if (pacmanLocation % 2 != 0) {
                pacman = 'c';
            }

            // here we move pacman forward, and replace the character where he was the loop before it with
            //      the given character
            double percentage;
            if (pacmanLocation == 0) {
                percentage = (double) 1 / length * 100;
                progressBar.setCharAt(0, pacman);
                allProgress[1] = formatBar(progressBar.toString(), percentage);
            } else {
                percentage = (double) (pacmanLocation + 1) / (length + 1) * 100;
                progressBar.setCharAt(pacmanLocation - 1, character);
                allProgress[pacmanLocation + 1] = formatBar(progressBar.toString(), percentage);

                progressBar.setCharAt(pacmanLocation, pacman);
                allProgress[pacmanLocation + 1] = formatBar(progressBar.toString(), percentage);
            }
        }

        // fill in index 0 and last index of the array with an empty bar and a completed bar
        String bar = repeatString(length, character);
        double percentage = 0;
        allProgress[0] = formatBar(bar, percentage);

        bar = repeatString(length, character).replaceAll("o", Character.toString(character));
        percentage = 100;
        allProgress[length + 1] = formatBar(bar, percentage);
        return allProgress;
    }

    public static void printProgress() {
        int length = findClosestNumber(21);

        // this part fills progressBars with all the different progress bars, and then backspaces the entire bar
        String[] progressBars = pacmanProgress(length, '-');
        for (String bar : progressBars) {
            System.out.print(bar);

            // after every printed bar the program sleeps so you can actually see pacman moving
            try {
                Thread.sleep(250);
                String removeLine = "";
                for (int i = 0; i < length + 8; i++) {
                    removeLine += "\b";
                }
                System.out.print(removeLine);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
